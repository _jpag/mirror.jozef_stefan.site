<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package brainblank
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="column-1">
			<img src="<?php echo get_template_directory_uri() ?>/images/logo-jozef-stefan.png" alt="">
		</div>
		<div class="column-2">
			<?php if (!dynamic_sidebar('footer') ) : endif; ?>
		</div>
		<div class="column-3">
			<a href="http://www.brainupstudio.it" target="_blank">
				<img src="<?php echo get_template_directory_uri() ?>/images/brainup-webdesign.svg" title="Powered by BrainUp - Grafica e Web design" alt="BrainUp - Grafica e Web design">
			</a>
		</div>
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package brainblank
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'brainblank' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
		<h1>
		<?php the_custom_logo(); ?>
		</h1>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
			) );
			// get_search_form();
			?>
		</nav><!-- #site-navigation -->
		<div class="c-nav__mobile js-navMobile">
				<span class="line line--1"></span>
				<span class="line line--2"></span>
				<span class="line line--3"></span>
				<span class="line line--4"></span>
			</div>
	</header><!-- #masthead -->
	
	<!-- widget calendar -->
	<!-- <div class="c-calendar">
		// aggiungere tag php -> if (!dynamic_sidebar('calendar')) : endif; ?>
	</div> -->
	<!-- end / widget calendar -->
	
	<!-- widget actionbar -->
	<div class="c-actionbar">
		<?php if (!dynamic_sidebar('actionbar') ) : endif; ?>
	</div><!-- end / widget actionbar -->
	
	<div id="content" class="site-content">

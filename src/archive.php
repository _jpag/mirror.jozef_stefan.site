<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package brainblank
 */

get_header();
if ( has_post_thumbnail() ) { 
	$image = wp_get_attachment_url( get_post_thumbnail_id() );
	$image_full = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', false );
	$image_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium', false );
	$image_full = $image_full[0];
} else {
   $image_full =   get_template_directory_uri() . '/images/img-news-default.jpg';
}
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>
				<!-- header -->
				<div class="c-page__header full" style="background-image:url('<?php echo $image_full ?>')">
					<div class="c-pageheader__inner l-container_page">
						<div class="c-pageheader__col1">
							<div class="c-pageheadercol1__inner archive">
								<?php the_archive_title( '<h1 class="c-page__title">', '</h1>' );
								the_archive_description( '<div class="archive-description">', '</div>' ); 
								?>
								
							</div>				
							<div class="c-page__breadcrumb filter">
								<?php
									if ( function_exists('yoast_breadcrumb') ) {
									yoast_breadcrumb('
									<p>. ','</p>
									');
									}
								?>
								<?php if (get_post_type() != 'amm-trasparente') : ?>
								<div class="c-news__filter">
								<a href="<?php echo home_url( $wp->request ) ?>/?order=desc"><?php pll_e('più recenti'); ?></a>
									<a href="<?php echo home_url( $wp->request ) ?>/?order=asc"><?php pll_e('più vecchi'); ?></a>
									<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
										<option value=""><?php echo esc_attr( __( 'Select Month' ) ); ?></option> 
										<?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
									</select>

									<?php 
									$args = array(
										'id' => 'categories-dropdown',
										'value_field' => 'slug',
										'show_option_none' => pll__('Seleziona categoria'),
									);
									wp_dropdown_categories($args); ?>
								</div>
								<?php endif ?>
							</div>
						</div>
					</div>
				</div>
				<!-- end / header -->
			<div class="c-news l-container"><?php 
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'news' );

			endwhile;
			?><div><?php
			
		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<div class="l-container">
	<?php the_posts_navigation(); ?>
</div>
<?php get_footer();
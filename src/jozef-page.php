<?php 
/** 
* Template Name: Pagina generica Jozef Stefan 
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
* @package brainblank
*/

get_header();
?>
	<section>
	<?php
		while ( have_posts() ) :
			the_post();
			if (is_front_page()) {
				get_template_part( 'template-parts/content', 'home-jozef' );
			} else {
				get_template_part( 'template-parts/content', 'page-jozef' );
			}
		endwhile; // End of the loop.
	?>
	</section>
<?php
get_footer();

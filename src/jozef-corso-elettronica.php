<?php 
/** 
* Template Name: Pagina Elettronica Jozef Stefan 
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
* @package brainblank
*/

get_header();
?>
	<section>
	<?php
		while ( have_posts() ) :
			the_post();
				get_template_part( 'template-parts/content', 'page-jozef' );
		endwhile; // End of the loop.

		?> <div class="l-container c-news"><?php
		$query = new WP_Query( array('posts_per_page' => 4, 'category_name' => 'elettronica, elektronika, electronics' ) );
		if ( $query->have_posts() ) { 
			while ( $query->have_posts() ) {
				$query->the_post();
				get_template_part( 'template-parts/content', 'news' );
			}
		}
		?>	
	</div>	
</section>
<?php
get_footer();

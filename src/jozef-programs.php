<?php 
/** 
* Template Name: Archivi download
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
* @package brainblank
*/

get_header();
?>
	<section>
	<?php
		while ( have_posts() ) :
			the_post();
			get_template_part( 'template-parts/content', 'programs-jozef' );
		endwhile; // End of the loop.
	?>
	</section>
<?php
get_footer();

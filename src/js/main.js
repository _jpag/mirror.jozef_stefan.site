(function ($) {

    var showreelInitializer = function () {
        var sl = $(".c-slideshow");
        if (sl.length > 0) {
            sl.cycle({
                timeout: 5000,
                speed: 800,
                next: '.next',
                prev: '.prev',
                slides: '.item',
                swipe: true,
                fx: 'fadeout',
                log: false,
                //swipeFx: 'scrollHorz',
                //pager: '.c-showreel__pager',
                pagerTemplate: '<a href="javascript:;"></a>',
                pagerActiveClass: 'isActive'
            });
        }
    };

    var carouselInitiazlier = function () {
        var el = $('.owl-carousel');
        if (el.length > 0) {
            el.owlCarousel({
                loop: false,
                margin: 20,
                nav: false,
                loop: true,
                autoplay: true,
                responsive: {
                    0: {
                        margin: 0,
                        items: 1
                    },
                    376: {
                        items: 2
                    },
                    768: {
                        items: 3
                    },
                    1024: {
                        items: 4
                    },
                    1440: {
                        items: 5
                    }
                }
            });
        }
    };

    var hideOnScroll = function (x) {
        var el = $('.js-hideOnScroll');
        var elH = el.innerHeight();
        console.log(elH);
        if (el.length > 0) {
            var pos = $(x).offset().top;
            $(window).scroll(function (event) {
                var scroll = $(window).scrollTop() + elH;
                el.toggleClass("isScrolled", scroll > pos);
            });
        }
    };

    var jsAppearInitializer = function () {
        /*
            Per attivare questa animazione basta aggiungere la classe js-appear a qualsiasi elemento.
            Se si vuole aggiungere un delay aggiungere un attributo "js-delay" es js-delay="150"
            (da utilizzare ad es. su elementi sulla stessa fila per non farli comparire tutti in blocco)
        */
        if ($(".js-appear").length > 0) {
            var viewPortHeight = $(window).height();
            var currPosition = $(window).scrollTop();

            var isOnScreen = function (el) {
                return ($(el).offset().top + 200) < (currPosition + viewPortHeight - ($(el).attr("js-delay") || 0));
            }
            $(".js-appear").each(function () {
                $(this).toggleClass("hidden", !isOnScreen(this));
            });
            var scrollHappened = function () {
                currPosition = $(window).scrollTop();
                $(".js-appear.hidden").each(function () {
                    $(this).toggleClass("hidden", !isOnScreen(this));
                });
            };

            $(window).scroll(scrollHappened);

            $(window).resize(function () {
                viewPortHeight = $(window).height()
                scrollHappened();
            });
        }

    };

    var togglerMenuMobile = function() {
        $(document).on("click", ".js-navMobile", function () {
            $('body').toggleClass("openMenu");
        });
    };

    var menuMobile = function(e) {
        var el = $('.js-noPage').children('a');
        var w = $(window).innerWidth();
        if (el.length > 0 && w < 1024) {
           el.on('click', function(e) {
                e.preventDefault();
                el.siblings('.sub-menu').removeClass('isOpen');
                $(this).siblings('.sub-menu').addClass('isOpen');
           });
        }
    };

    var anchorScroll = function() {
        var anchor = $('.js-scroll');
        var header = $('.site-header').innerHeight();
        var submenu = $('.c-anchor-nav');
        if (submenu.length > 0) {
            header = header + submenu.innerHeight();
        }
        if (anchor.length > 0) {
            anchor.click(function(){
                $('html, body').animate({
                    scrollTop: $( $(this).attr('href') ).offset().top - header
                }, 500);
                return false;
            });
                    // to top right away
        if ( window.location.hash ) scroll(0,0);
        // void some browsers issue
        setTimeout( function() { scroll(0,0); }, 1);

        $(function() {
            // *only* if we have anchor on the url
            if(window.location.hash) {

                // smooth scroll to the anchor id
                $('html, body').animate({
                    scrollTop: ($(window.location.hash).offset().top - header) + 'px'
                }, 1000, 'swing');
            }
        });
        }


    };

    var categoriesDropdwon = function(e) {
        var el = $('#categories-dropdown');
        var getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        console.log(baseUrl);
        if (el.length > 0) {
            el.change(function () {
                if ($(this).val()) {  
                    window.location = baseUrl + '/news/' + $(this).val();
                }
            });   
        }
    };

    var actionBarVisibility = function() {
        var el = $('.c-actionbar');
        if (el.length > 0) {
            $(window).scroll(function (event) {
                var scroll = $(window).scrollTop();
                el.toggleClass("isVisible", scroll > 100);
            });
        }
    };
    $(document).ready(function () {
        showreelInitializer();
        carouselInitiazlier();
        hideOnScroll('.c-lastnews');
        jsAppearInitializer();
        togglerMenuMobile();
        menuMobile();
        anchorScroll();
        categoriesDropdwon();
        actionBarVisibility();
    });
})(jQuery);
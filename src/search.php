<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package brainblank
 */

get_header();
if ( has_post_thumbnail() ) { 
	$image = wp_get_attachment_url( get_post_thumbnail_id() );
	$image_full = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', false );
	$image_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium', false );
	$image_full = $image_full[0];
} else {
   $image_full =   get_template_directory_uri() . '/images/img-news-default.jpg';
}
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>
				<!-- header -->
				<div class="c-page__header full" style="background-image:url('<?php echo $image_full ?>')">
					<div class="c-pageheader__inner l-container_page">
						<div class="c-pageheader__col1">
							<div class="c-pageheadercol1__inner archive">
							<h1 class="c-page__title">
								<?php
									/* translators: %s: search query. */
									printf( esc_html__( 'Search Results for: %s', 'brainblank' ), '<span>' . get_search_query() . '</span>' );
								?>
							</h1>		
							</div>	

							<div class="c-page__breadcrumb filter">
								<?php
									if ( function_exists('yoast_breadcrumb') ) {
									yoast_breadcrumb('
									<p>. ','</p>
									');
									}
								?>
							</div>
						</div>
					</div>
				</div>
				<!-- end / header -->

				<div class="c-page__container l-container_page"><?php 
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();

					/**
					 * Run the loop for the search to output the results.
					 * If you want to overload this in a child theme then include a file
					 * called content-search.php and that will be used instead.
					 */
					get_template_part( 'template-parts/content', 'search' );

				endwhile;

				the_posts_navigation();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif;
			?>
			</div>
		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();

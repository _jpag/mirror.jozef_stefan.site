<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package brainblank
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php
		// if (is_home()) {
		// 	the_title( '<h1 class="entry-title">', '</h1>' );
		// }
		the_content();

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'brainblank' ),
			'after'  => '</div>',
		) );
		?>
</div><!-- #post-<?php the_ID(); ?> -->

<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package brainblank
 */
if ( has_post_thumbnail() ) { 
	$image = wp_get_attachment_url( get_post_thumbnail_id() );
	$image_full = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', false );
	$image_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium', false );
	$image_full = $image_full[0];
} else {
   $image_full =   get_template_directory_uri() . '/images/img-news-default.jpg';
}
?>
<?php if (get_field('id_programs')) : ?>
	<div class="c-anchor-nav">
		<div class="inner l-container">
			<?php
				if( have_rows('programs') ):
					while ( have_rows('programs') ) : the_row(); 
						$id_nav = get_sub_field('id');
						$title_nav = get_sub_field('title');
					?>
					<a class="js-scroll" href="#<?php echo $id_nav; ?>"><?php echo $title_nav; ?></a>
					<?php endwhile;
				else :
				endif;
			?>
			<?php 
				$program_id = get_field('id_programs');
				$program_title = get_field('title_programs');
			?>
			<?php if ($program_id) : ?>
				<a class="js-scroll" href="#<?php echo $program_id; ?>"><?php echo $program_title; ?></a>
			<?php endif ?>
		</div>
	</div>
<?php endif ?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<!-- header -->
	<div class="c-page__header full" style="background-image:url('<?php echo $image_full ?>')">
		<div class="c-pageheader__inner l-container_page">
			<div class="c-pageheader__col1">
				<div class="c-pageheadercol1__inner">
					<?php the_title( '<h1 class="c-page__title">', '</h1>' ); ?>
				</div>				
				<div class="c-page__breadcrumb">
					<?php
						if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('
						<p>. ','</p>
						');
						}
					?>
				</div>
			</div>


		</div>
	</div>
	<!-- end / header -->

	<div class="c-programs__container">
		<div class="l-container_page">
			<?php
				if( have_rows('programs') ):
					while ( have_rows('programs') ) : the_row(); 
						$id = get_sub_field('id');
						$file = get_sub_field('file');
						$link = get_sub_field('link');
						$acf_image = get_sub_field('image');
						$image = wp_get_attachment_image_src( $acf_image['ID'], 'thumbnail', false );
						$title = get_sub_field('title');
						$text = get_sub_field('text');
					?>
					<div id="<?php echo $id; ?>" class="c-program__item">
						<div class="c-program__image" style="background-image: url('<?php echo $image[0]; ?>')"></div>
	
						<div class="c-program__content">
							<?php if ( $title ) : ?>
								<h4 class="title"><?php echo $title; ?></h4>
							<?php endif; ?> 
							<?php if ( $text ) : ?>
								<p><?php echo $text; ?></p>
							<?php endif; ?> 
							<?php if( $file ): ?>
								<p class="caption"><strong>DOWNLOAD: </strong><span><?php echo $file['filename']; ?></span></p>
								<a class="o-button_circle o-button_download" target="_blank" href="<?php echo $file['url']; ?>"></a>
							<?php endif; ?>  
							<?php if( $link ): ?>
								<p class="caption"><span><?php echo $link['title']; ?></span></p>
								<a class="o-button_circle" href="<?php echo $link['url']; ?>"></a>
							<?php endif; ?>  
						</div>
					</div>
					<?php endwhile;
				else :
				endif;
			?>
		</div>
	</div>

	<!-- page container -->
	<?php if (get_the_content()) : ?>
	<div id="<?php echo $program_id; ?>" class="c-page__container l-container_page">
		<?php the_content(); ?>
	</div>
	<?php endif ?>

	<!-- end / page container -->
	<?php
	
	wp_link_pages( array(
		'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'brainblank' ),
		'after'  => '</div>',
	) );
	?>
</div><!-- #post-<?php the_ID(); ?> -->

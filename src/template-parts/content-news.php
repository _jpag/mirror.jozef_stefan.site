<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package brainblank
 */
$image = wp_get_attachment_url( get_post_thumbnail_id() );
$image_full = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', false );
$image_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium', false );
$categories = get_the_category();
?>

<a class="c-news__item" href="<?php echo get_permalink() ?>">
<?php 
	if ($image_thumb[0]) { ?>
		<div class="c-newscarousel__img" style="background-image: url('<?php echo $image_thumb[0] ?>')"></div>
	<?php } ?>
	<div class="c-newscarousel__text">
		<p>
			<span class="c-newscarousel__info">
				<strong class="category"><?php if (get_post_type() != 'amm-trasparente') { echo 'NEWS';} else { echo 'AMMINISTRAZIONE TRASPARENTE';} ?> </strong>
				<strong><?php echo $categories[0]->name ?></strong>
			</span><br>
			<span><?php echo get_the_date(); ?></span>
		</p>
		<h4 class="title"><?php the_title(); ?></h4>
	</div>
	<div class="o-button_circle"></div>
</a><!-- #post-<?php the_ID(); ?> -->
<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package brainblank
 */
if ( has_post_thumbnail() ) { 
	$image = wp_get_attachment_url( get_post_thumbnail_id() );
	$image_full = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', false );
	$image_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium', false );
 } else {
	$image_full =   get_template_directory_uri() . '/images/img-news-default.jpg';
 }
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<!-- header -->
	<div class="c-page__header <?php if ( !has_post_thumbnail() ){ echo 'full'; } ?>" style="background-image:url('<?php if ( has_post_thumbnail() ){ 
		echo $image_full[0]; 
		} else { echo $image_full; } ?>')">
		<div class="c-pageheader__inner l-container_page">
			<div class="c-pageheader__col1 <?php if ( !has_post_thumbnail() ){ echo 'full'; } ?>">
				<div class="c-pageheadercol1__inner">
					<div>
						<div class="news"><?php if (get_post_type() != 'amm-trasparente') { echo 'NEWS';} else { echo 'AMMINISTRAZIONE TRASPARENTE';} ?>&nbsp;<span><?php echo the_category() ?></span></div>
						<?php the_title( '<h1 class="c-page__title">', '</h1>' ); ?>
					</div>				
					</div>	
				<div class="c-news__date">
				<?php echo the_date() ?>
				</div>			
			</div>
				<?php if ( has_post_thumbnail() ) {
					echo '<div class="c-pageheader__col2">';
					echo '<div class="c-pageheader__img" style="background-image:url('.$image_thumb[0].');"></div>';
					echo '</div>';
				} ?>
			
		</div>
	</div>
	<!-- end / header -->

	<!-- page container -->
	<div class="c-page__container l-container_page">
		<?php the_content(); ?>
	</div>
	<!-- end / page container -->
	<?php
	
	wp_link_pages( array(
		'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'brainblank' ),
		'after'  => '</div>',
	) );
	?>

</article><!-- #post-<?php the_ID(); ?> -->

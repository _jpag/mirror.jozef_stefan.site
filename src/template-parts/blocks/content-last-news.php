
<?php
	/**
	 * Block Name: News in evidenza
	 */
?>

<?php

	$args = array( 'numberposts' => '1' );
	$recent_posts = wp_get_recent_posts( $args );
	foreach( $recent_posts as $recent ){
		$categories = get_the_category($recent["ID"]);
		echo 
		'<div class="c-lastnews">
			<div class="c-lastnews__col1">
				<div class="image">			
					'. get_the_post_thumbnail($recent["ID"], medium) .'
				</div>
			</div>
			<div class="c-lastnews__col2">
				<div>
					<p class="c-lastnews__info"><strong class="category">NEWS </strong>&nbsp;<span>'. $categories[0]->name .' | '. date('j.m.Y', strtotime($recent['post_date'])) . '</span></p>
					<h3 class="title">' . $recent["post_title"] . '</h3>
					<p>' . $recent["post_excerpt"] . '</p>
				</div>
				<a class="o-button_circle" href="' . get_permalink($recent["ID"]) . '"></a> 
			</div>
		</div>';
	}
	wp_reset_query();
?>

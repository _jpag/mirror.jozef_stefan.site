<?php
    /**
     * Block Name: Fascia con testo in evidenza
     */
    $id = get_field('id');
    $file = get_field('file');
    $file = get_field('link');
    $image = get_field('image');
    $title = get_field('title');
    $text = get_field('text');
?>
<div id="<?php echo $id ?>" class="c-programs__item">
    <div class="inner" style="background-image: url('<?php echo $image; ?>')"></div>
    <?php if ( $title ) : ?>
        <h4><?php echo $title; ?></h4>
    <?php endif; ?> 
    <?php if ( $text ) : ?>
        <p><?php echo $text; ?></p>
    <?php endif; ?> 
    <?php if( $file ): ?>
        <span><?php echo $file['filename']; ?></span>
        <a class="o-button_circle" href="<?php echo $file['url']; ?>"></a>
    <?php endif; ?>  
    <?php if( $file ): ?>
        <a class="o-button_circle" href="<?php echo $link; ?>"></a>
    <?php endif; ?>  
</div>
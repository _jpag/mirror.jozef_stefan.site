
<?php
	/**
	 * Block Name: Fascia carosello news
	 */
?>

<div class="c-newscarousel">
	<div class="c-newscarousel__title">
		<?php if ( get_field('title') ) : ?>
			<h2 class="title"><?php echo get_field('title'); ?></h2>
		<?php endif; ?>
		
	</div>
	<div class="owl-carousel owl-theme">
	<?php
		$args = array( 'numberposts' => '6' );
		$recent_posts = wp_get_recent_posts( $args );
		foreach( $recent_posts as $recent ){
			$categories = get_the_category($recent["ID"]);
			echo '<a class="c-newscarousel__item item" href="' . get_permalink($recent["ID"]) . '">';
			if (has_post_thumbnail($recent["ID"])) {
				echo '<div class="c-newscarousel__img" style="background-image: url('. get_the_post_thumbnail_url($recent["ID"], thumbnail) .')"></div>';
			}
			echo '<div class="c-newscarousel__text">
					<p class="c-newscarousel__info"><strong class="category">NEWS </strong>&nbsp;<span>'. $categories[0]->name .' | '. date('j.m.Y', strtotime($recent['post_date'])) . '</span></p>
					<h4 class="title">' . $recent["post_title"] . '</h4>
				</div>
				<div class="o-button_circle"></div>
			</a>';
		}
		wp_reset_query();
	?>
	</div>
</div>
<?php
	/**
	 * Block Name: Showreel
	 */
?>
<?php if(get_field('slideshow')): ?>
	<section class="c-slideshow">
		<?php while(has_sub_field('slideshow')): ?>
			<div class="c-slideshow__slide item">
				<?php 
					$title = get_sub_field('title');
					$subtitle = get_sub_field('subtitle');
					$image = get_sub_field('image');
					$link = get_sub_field('link');
					$size = 'full'; // (thumbnail, medium, large, full or custom size)
					$alt = array('alt'=>$title);
				?>
				<div class="c-slideshow__image o-coverImage">
					<?php if( $image ) {
						echo wp_get_attachment_image($image, $size, false, $alt);
					} ?>
				</div>
				<div class="c-slideshow__content js-hideOnScroll">
					<h2><?php echo $title ?></h2>
					<h3><?php echo $subtitle ?></h3>
					<?php if ($link): ?>
						<a class="o-button o-button_light" target="<?php echo $link['target'] ?>" href="<?php echo $link['url'] ?>"><?php echo $link['title'] ?></a>
					<?php endif ?>
				</div>
			</div>
		<?php endwhile; ?>
	</section>
<?php endif; ?>


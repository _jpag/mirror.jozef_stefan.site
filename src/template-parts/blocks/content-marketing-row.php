<?php
    /**
     * Block Name: Fascia con testo in evidenza
     */
    $link = get_field('link');

?>
<div class="c-marketingrow">
    <div class="inner" style="background-image: url('<?php echo get_field('image'); ?>')">
    <?php if ( get_field('title') ) : ?>
        <h4 class="title"><?php echo get_field('title'); ?></h4>
    <?php endif; ?> 
    <?php if ( $link ) : ?>
			<a class="o-button_circle o-button_light" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"></a>
		<?php endif; ?> 
    </div>
</div>
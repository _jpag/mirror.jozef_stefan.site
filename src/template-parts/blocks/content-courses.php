<?php
    /**
     * Block Name: Riquadri corsi home page
     */
?>

<?php if ( have_rows('courses') ) : ?>
    <div class="c-homecourses">
        <div class="inner">
            <div class="c-homecourses__title">
                <?php if ( get_field('title') ) : ?>
                    <h2><?php echo get_field('title'); ?></h2>
                <?php endif; ?>
            </div>
            <?php while( have_rows('courses') ) : the_row(); ?>
                <div class="c-homecourses__item js-appear" style="background-image: url('<?php the_sub_field('image'); ?>')">
                    <h4><?php the_sub_field('title'); ?></h4>
                    <?php if ( get_sub_field('link') ) : ?>
                        <a class="o-button_circle o-button_light" href="<?php the_sub_field('link'); ?>"></a>
                    <?php endif; ?>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
<?php endif; ?>
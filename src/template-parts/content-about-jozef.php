<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package brainblank
 */

$image = wp_get_attachment_url( get_post_thumbnail_id() );
$image_full = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', false );
$image_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium', false );
?>
<div class="c-anchor-nav">
	<div class="inner l-container">
		<?php if (get_field('about_id') ): ?>
			<a class="js-scroll" href="#<?php echo get_field('about_id'); ?>"><?php echo get_field('title_about'); ?></a>
		<?php endif; ?> 
		<?php if (get_field('address_id') ): ?>
			<a class="js-scroll" href="#<?php echo get_field('address_id'); ?>"><?php echo get_field('address_title') ?></a>
		<?php endif; ?> 
		<?php if (get_field('id_form') ): ?>
			<a class="js-scroll" href="#<?php echo get_field('id_form') ?>"><?php echo get_field('form_title'); ?></a>
		<?php endif; ?> 
		<?php if (get_field('logos_id') ): ?>
			<a class="js-scroll" href="#<?php echo get_field('logos_id'); ?>"><?php echo get_field('logos_title') ?></a>
		<?php endif; ?> 
		<?php if (get_field('administration_id') ): ?>
			<a class="js-scroll" href="#<?php echo get_field('administration_id') ?>"><?php echo get_field('administration_title') ?></a>
		<?php endif; ?> 
		<?php if (get_field('media_id') ): ?>
			<a class="js-scroll" href="#<?php echo get_field('media_id'); ?>"><?php echo get_field('media_title') ?></a>
		<?php endif; ?> 
	</div>
</div>
<div id="post-<?php the_ID(); ?>" class="c-about">
	<!-- header -->
	<div class="c-page__header half" style="background-image:url('<?php echo $image_full[0] ?>')">
		<div class="c-pageheader__inner l-container_page">
			<div class="c-pageheader__col1">
				<div class="c-pageheadercol1__inner">
					<?php the_title( '<h1 class="c-page__title">', '</h1>' ); ?>
				</div>				
				<div class="c-page__breadcrumb">
					<?php
						if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('
						<p>. ','</p>
						');
						}
					?>
				</div>
			</div>
			<?php if ( has_post_thumbnail() ) {
					echo '<div class="c-pageheader__col2">';
					echo '<div class="c-pageheader__img" style="background-image:url('.$image_thumb[0].');"></div>';
					echo '</div>';
				} ?>
		</div>
	</div>
	<!-- end / header -->

	<!-- page container -->
	<div id="<?php echo get_field('about_id'); ?>" class="c-page__container l-container_page">
		<?php the_content(); ?>
	</div>
	<!-- end / page container -->

	<?php 
	
	?>
	<!-- acf ROW MKT content -->
	<div class="c-marketingrow">
		<?php $mkt_title = get_field('title') ?>
		<div class="inner" style="background-image: url('<?php echo get_field('image'); ?>')">
		<?php if ( $mkt_title ) : ?>
			<h4><?php echo $mkt_title; ?></h4>
		<?php endif; ?> 
		</div>
	</div>
	<!-- end / acf ROW MKT content -->

	<div class="l-container l-flex">
		<!-- acf ADDRESS content -->
		<?php 
			$addrs_title = get_field('address_title');
			$addrs_text = get_field('address_text');
		?>
		<div id="<?php echo get_field('address_id'); ?>" class="l-half c-about__address">
		<?php if ($addrs_title): ?>
			<div class="c-aboutaddress__title">
				<h2 class="title"><?php echo $addrs_title; ?></h2>	
			</div>	
		<?php endif; ?>	
		<?php if ($addrs_text): ?>
			<div class="c-aboutaddress__text">
				<?php echo $addrs_text; ?>
			</div>	
		<?php endif; ?>		

		</div>	
		<!-- end / acf ADDRESS content -->
		<!-- acf MAP content -->
		<?php $map = get_field('maps'); ?>	
		<?php if ($map): ?>
			<div id="map" class="l-half"></div>
		<?php endif; ?>	
		<!-- end / acf MAP content -->
	</div>

	<!-- acf FORM content -->
	<div id="<?php echo get_field('id_form') ?>" class="c-about__contact l-container">
		<?php 
			$form = get_field('form');
		?>
		<div class="c-aboutcontact__image" style="background-image: url('<?php echo get_field('contact_image') ?>')"></div>
		<div class="c-about__form">
			<div>
				<h2 class="title"><?php echo get_field('form_title'); ?></h2>	
			</div>
			<?php if ($form): ?>
				<?php echo $form ?>
			<?php endif; ?>	
		</div>
	</div>
	<!-- end / acf FORM content -->

	<!-- acf PIANO TRIENNALE content -->
	<?php if (get_field('logos')): ?>
		<div id="<?php echo get_field('logos_id'); ?>" class="c-logos l-container">
			<div class="title"><h2><?php echo get_field('logos_title') ?></h2></div>
			<div class="inner">
				<?php while(has_sub_field('logos')): 
					$logo = get_sub_field('logo');
				?>
				<?php if( !empty($logo) ): ?>
					<div class="c-logos__item">
						<img width="200" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['name']; ?>" />
					</div>
				<?php endif; ?>				
				<?php endwhile; ?>
			</div>
		</div>
	<?php endif; ?>
	<!-- end / acf PIANO TRIENNALE content -->


	<?php 
			$admin_image = get_field('administration_image');
			$admin_title = get_field('administration_title');
			$admin_link = get_field('administration_link');
	?>
	<?php if ($admin_title) : ?>
	<!-- acf ROW 1 content -->
	<div id="<?php echo get_field('administration_id') ?>" class="c-marketingrow">
		
		<div class="inner" style="background-image: url('<?php echo $admin_image; ?>')">
		<?php if ( admin_title ) : ?>
			<h2 class="title"><?php echo $admin_title; ?></h2>
		<?php endif; ?> 

		<?php if ( $admin_link ) : ?>
			<a class="o-button_circle o-button_light" href="<?php echo $admin_link['url']; ?>" target="<?php echo $admin_link['target']; ?>"></a>
		<?php endif; ?> 
		</div>
	</div>
	<!-- end / acf ROW 1 content -->
	<?php endif; ?>

	<!-- acf SCUOLA NEI MEDIA content -->
	<?php if(get_field('citazioni')): ?>
		<div id="<?php echo get_field('media_id'); ?>" class="c-mediapress l-container">
			<div class="title"><h2><?php echo get_field('media_title') ?></h2></div>
			<div class="inner">
				<?php while(has_sub_field('citazioni')): 
					$quote = get_sub_field('title');
					$by = get_sub_field('testata');
				?>
				<blockquote>		
					<?php if ($quote): ?>
						<p><?php echo $quote ?></p>
					<?php endif; ?>
					<?php if ($by): ?>
						<i><p><?php echo $by ?></p></i>
					<?php endif; ?>
				</blockquote>
				<?php endwhile; ?>
			</div>
		</div>
	<?php endif; ?>
	<!-- end / acf SCUOLA NEI MEDIA content -->

	<?php	
	wp_link_pages( array(
		'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'brainblank' ),
		'after'  => '</div>',
	) );
	?>
</div><!-- #post-<?php the_ID(); ?> -->


<script>
	function initMap() {
	var Jozef = {lat: parseFloat('<?php echo $map['lat'] ?>'), lng: parseFloat('<?php echo $map['long'] ?>')};
	var styledMapType = new google.maps.StyledMapType(
		[
			{
				"featureType": "all",
				"stylers": [
					{
						"saturation": 0
					},
					{
						"hue": "#e7ecf0"
					}
				]
			},
			{
				"featureType": "road",
				"stylers": [
					{
						"saturation": -70
					}
				]
			},
			{
				"featureType": "transit",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "poi",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "water",
				"stylers": [
					{
						"visibility": "simplified"
					},
					{
						"saturation": -60
					}
				]
			}
		],
		{name: 'Styled Map'});
	var map = new google.maps.Map(
		document.getElementById('map'), {zoom: 15, center: Jozef, disableDefaultUI: true});
		map.mapTypes.set('styled_map', styledMapType);
		map.setMapTypeId('styled_map');
	var marker = new google.maps.Marker({position: Jozef, map: map});
	}
</script>

<script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBrCDdwb9CpTMKo50WxZBTpr49i9LEa-gY&callback=initMap">
</script>
